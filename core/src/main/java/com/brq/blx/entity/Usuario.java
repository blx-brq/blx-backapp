package com.brq.blx.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "BLX_USUARIO")
@NamedQuery(name = "Usuario.findAll", query = "SELECT b FROM Usuario b")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Solução do problema referente a sequência do Hibernate: Primeiramente
	 * você deve colocar a anotação [ allocationSize = 1 ] dentro da
	 * anotação @SequenceGenerator, caso isso não resolva, troque manualmente o
	 * valor do incrementBy dentro do SQLDEVELOPER
	 */

	/*
	 * OBSERVE: ALGUMAS COLUNAS QUE DEVERIAM POSSUIR A ATRIBUIÇÃO "UNIQUE" NÃO
	 * POSSUEM A CARACTERÍSTICA DE VALOR "ÚNICO", POIS ESTAMOS EM FASE DE TESTE
	 * LOGO MAIS ADICIONAREMOS NAS ANOTAÇÕES @Column a anotação: (unique = true)
	 */

	@Id
	@SequenceGenerator(name = "BLX_USUARIO_CODUSUARIO_GENERATOR", sequenceName = "BLXSEQ_ID_USUARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLX_USUARIO_CODUSUARIO_GENERATOR")
	@Column(name = "COD_USUARIO")
	@Expose
	private long codUsuario;

	@Column(name = "NM_NOME")
	@Expose
	private String nmNome;

	@Column(name = "VL_EMAIL")
	@Expose
	private String vlEmail;

	@Column(name = "VL_LOGIN")
	@Expose
	private String vlLogin;
 
	@Column(name = "VL_CPF")
	@Expose
	private String vlCpf;

/*	Cara, isso aqui vai precisar ser mudado na base de dados, deleta os dados e da um ALTER TABLE BLX_USUARIO MODIFY (VL_SKYPE NULL);*/
	@Column(name = "VL_SKYPE")
	@Expose
	private String vlSkype;


	@Column(name = "VL_SENHA")
	@Expose
	private String vlSenha;

	@Column(name = "VL_STATUS")
	@Expose
	private Integer vlStatus;

	/* RELAÇÕES JOINCOLUMN */

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TIPOUSUARIO_COD_TIPOUSUARIO")
	@Expose
	private TipoUsuario blxTipousuario;

	/* Relações */

	@OneToMany(mappedBy = "blxUsuario")
	@Expose
	private List<Anuncio> blxAnuncios;

	@OneToMany(mappedBy = "blxUsuario")
	@Expose
	private List<Contato> blxContatos;

	public Usuario() {
	}

	public Usuario(long codUsuario, String nmNome, String vlEmail, String vlLogin, String vlCpf, String vlSkype,
			String vlSenha, Integer vlStatus, TipoUsuario blxTipousuario) {
		super();
		this.codUsuario = codUsuario;
		this.nmNome = nmNome;
		this.vlEmail = vlEmail;
		this.vlLogin = vlLogin;
		this.vlCpf = vlCpf;
		this.vlSkype = vlSkype;
		this.vlSenha = vlSenha;
		this.vlStatus = vlStatus;
		this.blxTipousuario = blxTipousuario;
	}

	public long getCodUsuario() {
		return this.codUsuario;
	}

	public void setCodUsuario(long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getNmNome() {
		return this.nmNome;
	}

	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}

	public String getVlEmail() {
		return this.vlEmail;
	}

	public void setVlEmail(String vlEmail) {
		this.vlEmail = vlEmail;
	}

	public String getVlLogin() {
		return this.vlLogin;
	}

	public void setVlLogin(String vlLogin) {
		this.vlLogin = vlLogin;
	}

	public String getVlCpf() {
		return vlCpf;
	}

	public void setVlCpf(String vlCpf) {
		this.vlCpf = vlCpf;
	}

	public String getVlSkype() {
		return vlSkype;
	}

	public void setVlSkype(String vlSkype) {
		this.vlSkype = vlSkype;
	}

	public List<Anuncio> getBlxAnuncios() {
		return blxAnuncios;
	}

	public void setBlxAnuncios(List<Anuncio> blxAnuncios) {
		this.blxAnuncios = blxAnuncios;
	}

	public List<Contato> getBlxContatos() {
		return blxContatos;
	}

	public void setBlxContatos(List<Contato> blxContatos) {
		this.blxContatos = blxContatos;
	}

	public String getVlSenha() {
		return this.vlSenha;
	}

	public void setVlSenha(String vlSenha) {
		this.vlSenha = vlSenha;
	}

	public Integer getVlStatus() {
		return this.vlStatus;
	}

	public void setVlStatus(Integer vlStatus) {
		this.vlStatus = vlStatus;
	}

	public TipoUsuario getBlxTipousuario() {
		return blxTipousuario;
	}

	public void setBlxTipousuario(TipoUsuario blxTipousuario) {
		this.blxTipousuario = blxTipousuario;
	}

	@Override
	public String toString() {
		return "{'codUsuario':" + this.codUsuario + ",'nmNome':" + this.nmNome + ",'vlEmail':" + this.vlEmail
				+ ",'vlLogin':" + this.vlLogin + ",'vlCpf':" + this.vlCpf + ",'vlSenha':" + this.vlSenha + ",'vlStatus':"
				+ this.vlStatus + ",'blxTipoUsuario':" + this.blxTipousuario + "'vlSkype':" + this.vlSkype +"}";
	}
}