package com.brq.blx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "BLX_AVALIACAO")
@NamedQuery(name = "Avaliacao.findAll", query = "SELECT b FROM Avaliacao b")
public class Avaliacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BLX_AVALIACAO_CODAVALIACAO_GENERATOR", sequenceName = "SEQ_ID_AVALIACAO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLX_AVALIACAO_CODAVALIACAO_GENERATOR")
	@Column(name = "COD_AVALIACAO")
	@Expose
	private long codAvaliacao;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_AVALIACAO")
	@Expose
	private Date dtAvaliacao;

	@Column(name = "VL_NOTA")
	@Expose
	private String vlNota;
	
	/* Relações JOINCOLUMN */

	@ManyToOne
	@JoinColumn(name="ANUNCIO_COD_ANUNCIO")  
	private Anuncio blxAnuncio;

	public Avaliacao() {}

	public Avaliacao(long codAvaliacao, Date dtAvaliacao, String vlNota, Anuncio blxAnuncio) {
		super();
		this.codAvaliacao = codAvaliacao;
		this.dtAvaliacao = dtAvaliacao;
		this.vlNota = vlNota;
		this.blxAnuncio = blxAnuncio;
	}

	public long getCodAvaliacao() {
		return this.codAvaliacao;
	}

	public void setCodAvaliacao(long codAvaliacao) {
		this.codAvaliacao = codAvaliacao;
	}

	public Date getDtAvaliacao() {
		return this.dtAvaliacao;
	}

	public void setDtAvaliacao(Date dtAvaliacao) {
		this.dtAvaliacao = dtAvaliacao;
	}

	public String getVlNota() {
		return this.vlNota;
	}

	public void setVlNota(String vlNota) {
		this.vlNota = vlNota;
	}

	@Override
	public String toString() {
		return "Avaliacao [codAvaliacao=" + codAvaliacao + ", dtAvaliacao=" + dtAvaliacao + ", vlNota=" + vlNota
				+ ", blxAnuncio=" + blxAnuncio + "]";
	}
}