package com.brq.blx.entity;

import java.io.Serializable;
import javax.persistence.*;
import com.google.gson.annotations.Expose;

import java.util.List;

@Entity
@Table(name = "BLX_CATEGORIA")
@NamedQuery(name = "Categoria.findAll", query = "SELECT b FROM Categoria b")
public class Categoria implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BLX_CATEGORIA_CODCATEGORIA_GENERATOR", sequenceName = "SEQ_ID_CATEGORIA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLX_CATEGORIA_CODCATEGORIA_GENERATOR")
	@Column(name = "COD_CATEGORIA")
	@Expose
	private Long codCategoria;

	@Column(name = "DS_DESCRICAO")
	@Expose
	private String dsDescricao;

	@Column(name = "NM_NOME")
	@Expose
	private String nmNome;
	
	@OneToMany(mappedBy="blxCategoria")
	private List<Anuncio> anuncios;

	/* RELAÇÕES JOINCOLUMN  */
	
	@ManyToOne(targetEntity=Categoria.class)
	@JoinColumn(name = "CATEGORIA_COD_CATEGORIA")
	
	private Categoria blxCategoriaPai;

	public Categoria() {}

	public Long getCodCategoria() {
		return this.codCategoria;
	}

	public void setCodCategoria(Long codCategoria) {
		this.codCategoria = codCategoria;
	}

	public String getDsDescricao() {
		return this.dsDescricao;
	}

	public void setDsDescricao(String dsDescricao) {
		this.dsDescricao = dsDescricao;
	}

	public String getNmNome() {
		return this.nmNome;
	}

	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}

	public List<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(List<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}

	@Override
	public String toString() {
		return "{'codCategoria':" + this.codCategoria + ",'dsDescricao':" + this.dsDescricao + 
				",'nmNome':" + this.nmNome + "}";
	}
}