package com.brq.blx.persistence;

import java.util.List;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Avaliacao;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class AvaliacaoDao extends AbstractRepository<Avaliacao> {

	
	public List<Avaliacao> buscarAvaliacoes (Integer id) throws Exception
	{
		@SuppressWarnings("unchecked")
		List<Avaliacao> lista = this.entityManager.createQuery("FROM Avaliacao WHERE "
				+ "blxAnuncio.codAnuncio = :id").setParameter("id", id).getResultList();
		
		return lista;
	}

	@Override
	public List<Avaliacao> buscarPorFiltro(Avaliacao entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}