package com.brq.blx.persistence;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;

import com.brq.blx.entity.Usuario;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class UsuarioDao extends AbstractRepository<Usuario> {
	
	public boolean autenticar (Usuario usuario) throws Exception {
		@SuppressWarnings("unchecked")
		List<Usuario> usuarios = this.entityManager.createQuery("FROM Usuario WHERE "
				+ "vlLogin = :login AND vlSenha = "
				+ ":senha").setParameter("login",usuario.getVlLogin())
				.setParameter("senha", usuario.getVlSenha()).getResultList();
		
		if(usuarios.size() > 0 ) {
			return true;
		}
		
		return false;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> buscarPorFiltro(Usuario user) {
		Boolean whereAdd = false;
		StringBuilder sb = new StringBuilder("FROM Usuario u ");
		
		if(user.getNmNome() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			sb.append("u.nmNome like :nome ");
		}
		
		if(user.getVlEmail() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" u.vlEmail like :email ");
		}
		
		if(user.getVlLogin() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" u.vlLogin like :login ");
		}
		if(user.getVlCpf() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" u.vlRg like :rg ");
		}
		if(user.getVlStatus() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" u.vlStatus like :status ");
		}
		
		if(user.getBlxTipousuario() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append("u.blxTipousuario like :tipoUsuario ");
		}
				
		Query query = this.entityManager.createQuery(sb.toString());
		
		if(user.getNmNome() != null)
		{
			query.setParameter("nome", user.getNmNome());
		}
		
		if(user.getVlEmail() != null)
		{
			query.setParameter("email", user.getVlEmail());
		}
		
		if(user.getVlLogin() != null)
		{
			query.setParameter("login", user.getVlLogin());
		}
		
		if(user.getVlCpf() != null)
		{
			query.setParameter("rg", user.getVlCpf());
		}
		
		if(user.getVlStatus() != null)
		{
			query.setParameter("status", user.getVlStatus());
		}
		
		if(user.getBlxTipousuario() != null)
		{
			query.setParameter("blxTipousuario", user.getBlxTipousuario().getVlTipo());
		}
		
		return query.getResultList();
			
	}


	
	
	

}