package com.brq.blx.persistence;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;
import com.brq.blx.entity.Anuncio;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class AnuncioDao extends AbstractRepository<Anuncio> {

	@SuppressWarnings("unchecked")
	public List<Anuncio> buscarMeusAnuncios(Integer id) {
		return this.entityManager.createQuery("FROM " + this.entityType + 
									   " WHERE blxUsuario.codUsuario = ?").getResultList();
	}
	
	public List<Anuncio> buscarPorCategoria(Integer id) throws Exception
	{
		@SuppressWarnings("unchecked")
		List<Anuncio> listaAnuncio = this.entityManager.createQuery("FROM Anuncio a WHERE "
				+ "a.blxCategoria.codCategoria = :id").setParameter("id", id).getResultList();
		
		return listaAnuncio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Anuncio> buscarPorFiltro(Anuncio anuncio) {
		Boolean whereAdd = false;
		StringBuilder sb = new StringBuilder("FROM Anuncio a ");
		
		if(anuncio.getNmNome() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			sb.append("a.nmNome like :nome ");
		}
		
		if(anuncio.getDsDescricao() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" a.dsDescricao like :descricao ");
		}
		
		if(anuncio.getBlxUsuario() != null){
			if(!whereAdd){
				sb.append("WHERE ");
				whereAdd = true;
			}
			else{
				sb.append("AND ");
			}
			sb.append(" a.blxUsuario.nmNome like :usuario ");
		}
		
		
		Query query = this.entityManager.createQuery(sb.toString());
		
		if(anuncio.getNmNome() != null)
		{
			query.setParameter("nome", anuncio.getNmNome());
		}
		
		if(anuncio.getDsDescricao() != null)
		{
			query.setParameter("descricao", anuncio.getDsDescricao());
		}
		
		
		if(anuncio.getBlxUsuario() != null)
		{
			query.setParameter("blxTipousuario", anuncio.getBlxUsuario().getNmNome());
		}
		
		return query.getResultList();
			
	}
	
	
}